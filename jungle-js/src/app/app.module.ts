import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SimulationPage } from '../pages/simulation/simulation';

import {PixiModule} from 'angular2pixi'

import { StompService } from 'ng2-stomp-service';

//declare module 'stompjs';
//declare module 'sockjs-client';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SimulationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    PixiModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SimulationPage
  ],
  providers: [
    StatusBar,
    StompService,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
