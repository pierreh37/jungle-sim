import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {SimulationPage} from "../simulation/simulation";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  public goToSim(event,url){
    this.navCtrl.push(SimulationPage,{
      url:url
    });
  }

}
