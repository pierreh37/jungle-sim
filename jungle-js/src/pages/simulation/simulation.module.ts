import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SimulationPage } from './simulation';
import {PixiModule} from 'angular2pixi'


@NgModule({
  declarations: [
    SimulationPage,
  ],
  imports: [
    IonicPageModule.forChild(SimulationPage),PixiModule
  ],
})
export class SimulationPageModule {}
