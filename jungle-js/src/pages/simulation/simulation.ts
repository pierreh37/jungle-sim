import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PixiService} from 'angular2pixi'
import * as PIXI from 'pixi.js';
import {StompService} from "ng2-stomp-service";

interface Shape{
  posx:number,
  posy:number,
  id:string,
  shape:string,
  size:number,
  r:number,
  g:number,
  b:number
}

interface ShapeList{
  shapeList:Shape[];
}

@IonicPage()
@Component({
  selector: 'page-simulation',
  templateUrl: 'simulation.html',
})
export class SimulationPage {
  @ViewChild('mainScreen') divScreen: ElementRef;
  resizing:boolean=false;
  private subscription: any;

  private shapes:ShapeList;
  private shapeMap: { [id: string] : any; } = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pixi:PixiService,
    public stomp: StompService) {
    let url = this.navParams.get("url");
    //configuration
    stomp.configure({
      host:url,
      //'http://junglewar.test.rphstudio.net/jungle-war/gs-guide-websocket'
      //host:'http://localhost:8080/jungle-war/gs-guide-websocket',
      headers: {
        login: 'user1',
        passcode: '1user'
      },
      debug:false,//set to true for STOM message logging
      queue:{'init':false}
    });

    //start connection
    stomp.startConnect().then(() => {
      stomp.done('init');
      console.log('connected');

      //send data

      stomp.subscribe('/user/topic/jungle/answer', this.processServerAnswer);
      stomp.send('/app/control',{data:"init"});


      //unsubscribe
      //this.subscription.unsubscribe();

      //disconnect
      /*stomp.disconnect().then(() => {
        console.log( 'Connection closed' )
      })*/
    });
  }

  //response
  public processServerAnswer = (data) => {
    console.log(data);
    let dimension = data.result;
    var graphics = new PIXI.Graphics();
    graphics.beginFill(0x808000);//00RRGGBB
    graphics.drawRect(0, 0, dimension.x, dimension.y);
    this.pixi.worldStage.addChild(graphics);
    //subscribe
    this.subscription = this.stomp.subscribe('/topic/jungle/state', this.processStateMessage);
  }

  //response
  public processStateMessage = (data) => {
    this.shapes=data;
  }

  rgbToInt(r:number,g:number,b:number){
    if(r>1.0){
      r=1;
      console.error("red out of range");
    }
    if(g>1.0){
      g=1;
      console.error("green out of range");
    }
    if(b>1.0){
      b=1;
      console.error("blue out of range");
    }

    if(r<0.0){
      r=0;
      console.error("red out of range");
    }
    if(g<0.0){
      g=0;
      console.error("green out of range");
    }
    if(b<0.0){
      b=0;
      console.error("blue out of range");
    }
    r*=0xFF;
    g*=0xFF;
    b*=0xFF;
    return ((r<<16)&0xFF0000)|((g<<8)&0xFF00)|(b&0xFF);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SimulationPage');
    this.resizeToDiv();
    this.pixi.app.ticker.add(()=>{
      if(this.shapes!=undefined) {
        let shapes: Shape[] = this.shapes.shapeList;
        let serverIds:string[] = [];
        shapes.forEach((shape: Shape) => {
          serverIds.push(shape.id);
          let graph = this.shapeMap[shape.id];
          if (graph == undefined) {
            var graphics = new PIXI.Graphics();
            graphics.beginFill(this.rgbToInt(shape.r, shape.g, shape.b));
            graphics.drawRect(-shape.size/2, -shape.size/2, shape.size, shape.size);
            this.pixi.worldStage.addChild(graphics);
            //create
            this.shapeMap[shape.id] = graphics;
            graph = graphics;
          }
          graph.x = shape.posx;
          graph.y = shape.posy;
        });

        let shapeMapIds:string[] = Object.keys(this.shapeMap);
        shapeMapIds.forEach((value:string, index) => {
          if(serverIds.indexOf(value)==-1){
            let graphics = this.shapeMap[value];
            this.pixi.worldStage.removeChild(graphics);
            delete this.shapeMap[value];
          }
        });
      }
    });
  }



  pixiInit(){
    console.log("PH default oninit");
  }

  resizeToDiv() {
    if (!this.resizing) {
      this.resizing = true;
      setTimeout(() => {
        let newWidth = this.divScreen.nativeElement.offsetWidth;
        let newHeight = this.divScreen.nativeElement.offsetHeight;
        console.log('window resize', newHeight, newWidth);
        this.pixi.renderer.resize(newWidth, newHeight);
        this.resizing = false;
      }, 300);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event){
    this.resizeToDiv();
  }

}
