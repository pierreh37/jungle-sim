rmdir /s/q .\www\
xcopy /s .\..\build\libs .\www\
cd www
ren *.war jungle-war.war
cd ..
docker build -t webserver-jungle-war .
docker image save webserver-jungle-war:latest -o webserver-jungle-war.docker_image