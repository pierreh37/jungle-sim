package fr.rphstudio.jungle.spring.service.simulation;

import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.simulation.JungleSimulation;
import fr.rphstudio.jungle.simulation.dto.DataRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class JungleControlerConfiguration {

    public static final String chanelControl = "/control";// prefixed by /app (hidden)
    public static final  String chanelUseRresponse = "/topic/jungle/answer";

    @Autowired
    private SimpMessagingTemplate template;


    @Autowired
    private JungleConfiguration jungle;

    @MessageMapping(chanelControl)
    @SendToUser(chanelUseRresponse) // <- maps to "/user/"+userResponse
    public DataRequest serverProcess(DataRequest message) {
        JungleSimulation jungleSimulation = jungle.getJungleSimulation();
        int height = jungleSimulation.getHeight();
        int width = jungleSimulation.getWidth();
        message.setResult(new Vector2(width,height));
        return message;
    }

}
