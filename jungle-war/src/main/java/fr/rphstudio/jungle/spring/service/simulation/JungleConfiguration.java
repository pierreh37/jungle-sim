package fr.rphstudio.jungle.spring.service.simulation;

import fr.rphstudio.jungle.simulation.JungleSimulation;
import fr.rphstudio.jungle.simulation.dto.SimulationStateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class JungleConfiguration {

    private JungleSimulation jungleSimulation;
    public static final String chanelState = "/topic/jungle/state";
    //Gson gson = new Gson();

    /*@MessageMapping("/control")
    @SendToUser(chanelUseRresponse) // <- maps to "/user/"+userResponse
    public DataRequest serverProcess(DataRequest message) throws Exception {
        System.out.println(message.getData());
        return new DataRequest(message+"!!!");
    }*/

    @Autowired
    private SimpMessagingTemplate template;

    private long timePerSimulationStep = 20;
    private long timePerSimulationStatusUpdate = 20;

    private long lastTimeSimulationStep = System.currentTimeMillis();
    private long lastTimeSimulationStatusUpdate = System.currentTimeMillis();
    Thread simulationThread;

    @PostConstruct
    public void init() {
        setJungleSimulation(new JungleSimulation());
        getJungleSimulation().create();
        lastTimeSimulationStep = System.currentTimeMillis();
        lastTimeSimulationStatusUpdate = System.currentTimeMillis();
        simulationThread = new Thread(() -> {
            while (true) {
                long currentTimeMillis = System.currentTimeMillis();
                if (lastTimeSimulationStep + timePerSimulationStep < currentTimeMillis) {
                    lastTimeSimulationStep = currentTimeMillis;
                    getJungleSimulation().step();
                }

                if (lastTimeSimulationStatusUpdate + timePerSimulationStatusUpdate < currentTimeMillis) {
                    lastTimeSimulationStatusUpdate = currentTimeMillis;
                    SimulationStateDTO dto = getJungleSimulation().getDTO();
                    template.convertAndSend(chanelState, dto);
                }

                //estimate time to sleep
                currentTimeMillis = System.currentTimeMillis();
                long minMsTime = Math.min(lastTimeSimulationStep + timePerSimulationStep, lastTimeSimulationStatusUpdate + timePerSimulationStatusUpdate);
                long estimateTimeToSleep = minMsTime - currentTimeMillis;
                if (estimateTimeToSleep >= 1) {
                    try {
                        Thread.sleep(estimateTimeToSleep);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        throw new Error(e);
                    }
                }
            }
        });

        simulationThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
                System.out.println("Uncaught exception: " + e);
            }
        });
        simulationThread.setDaemon(true);
        simulationThread.start();
        System.out.println("SIMULATION STARTED");
    }

    public JungleSimulation getJungleSimulation() {
        return jungleSimulation;
    }

    public void setJungleSimulation(JungleSimulation jungleSimulation) {
        this.jungleSimulation = jungleSimulation;
    }

}
