package fr.rphstudio.jungle.simulation.core.actuator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import fr.rphstudio.jungle.simulation.core.state.BodyState;
import org.tensorflow.Tensor;

public class Move implements Actuator{
    BodyState bodyState;

    public Move(BodyState bodyState) {
        this.bodyState = bodyState;
    }

    public void move(Vector2 velocity){
        bodyState.getBody().setLinearVelocity(velocity);
    }

    @Override
    public void doAction(Tensor data) {

    }
}
