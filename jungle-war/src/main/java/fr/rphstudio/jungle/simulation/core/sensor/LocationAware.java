package fr.rphstudio.jungle.simulation.core.sensor;

import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.simulation.core.state.BodyState;
import org.tensorflow.Tensor;

public class LocationAware implements Sensor{

    private BodyState bodyState;

    public LocationAware(BodyState bodyState) {
        this.bodyState = bodyState;
    }

    public Vector2 getLocation(){
        return bodyState.getBody().getPosition();
    }

    @Override
    public Tensor getValue() {
        return null;
    }
}
