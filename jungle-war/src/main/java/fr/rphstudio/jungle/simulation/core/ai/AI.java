package fr.rphstudio.jungle.simulation.core.ai;

import fr.rphstudio.jungle.simulation.core.actuator.Actuator;
import fr.rphstudio.jungle.simulation.core.sensor.Sensor;

import java.util.List;
import java.util.Map;

public interface AI {
    public void step(Map<Class,List<Sensor>> sensors, Map<Class,List<Actuator>> actuators);

}
