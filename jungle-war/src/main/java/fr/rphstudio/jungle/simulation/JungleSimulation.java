package fr.rphstudio.jungle.simulation;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import fr.rphstudio.jungle.simulation.core.StateRule.*;
import fr.rphstudio.jungle.simulation.core.bot.AiBot;
import fr.rphstudio.jungle.simulation.core.bot.AiBotFactory;
import fr.rphstudio.jungle.simulation.core.resource.Resource;
import fr.rphstudio.jungle.simulation.core.resource.ResourceFactory;
import fr.rphstudio.jungle.simulation.core.state.BodyState;
import fr.rphstudio.jungle.simulation.core.state.State;
import fr.rphstudio.jungle.simulation.dto.ShapeDTO;
import fr.rphstudio.jungle.simulation.dto.SimulationStateDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class JungleSimulation implements EnvironmentAccessor {
    //set the world rules:
    // time and space limit
    // boundary / moving capacity etc
    //Send display informations multi viewer (TODO with tomcat later)
    //manager bot states (body)
    // manage environemental ressources spawning
    // manage genetics selection (bot spawning / reproduction)
    // rule bot must colect resources to survive


    World world;
    List<AiBot> bots = new ArrayList<>();
    List<Resource> resources = new ArrayList<>();

    List<BotRule> botRules = new ArrayList<>();
    List<WorldRule> worldRule = new ArrayList<>();

    int width = 640;
    int height = 480;

    AiBotFactory aiBotFactory;
    ResourceFactory resourceFactory;

    public void create() {
        //http://www.gamefromscratch.com/post/2014/08/27/LibGDX-Tutorial-13-Physics-with-Box2D-Part-1-A-Basic-Physics-Simulations.aspx
        world = new World(new Vector2(0, 0), true);

        aiBotFactory = new AiBotFactory(this);
        resourceFactory = new ResourceFactory(this);

        botRules.add(new DieFromHunger(this));
        PhysicLaw physicLaw = new PhysicLaw(this);
        botRules.add(physicLaw);

        worldRule.add(physicLaw);
        worldRule.add(new LifeRule());
        worldRule.add(new ResourceRule());
    }

    public void step(){
        world.step(1, 6, 2);
        ListIterator<AiBot> botListIterator = bots.listIterator();
        for (AiBot bot : bots) {
            for (BotRule stateRule : botRules) {
                stateRule.step(bot);
            }
            bot.step();
        }
        for (Resource resource : resources) {
            resource.step();
        }


        List<AiBot> removeList = aiBotFactory.getRemoveList();
        for (AiBot bot : removeList) {
            bots.remove(bot);
        }
        removeList.clear();

        for (WorldRule rule : worldRule) {
            rule.step(this);
        }
    }

    public SimulationStateDTO getDTO(){
        ArrayList<ShapeDTO> ret = new ArrayList<>();
        for (AiBot bot : bots) {
            for (State state : bot.getStates().values()) {
                if(state instanceof BodyState){
                    BodyState bodyState = (BodyState) state;
                    ret.add(bodyState.getShape().convert());
                }
            }
        }
        for (Resource resource : resources) {
            ret.add(resource.getShape().convert());
        }
        return new SimulationStateDTO(ret);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<AiBot> getBots() {
        return bots;
    }

    public World getPhysicWorld() {
        return world;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public AiBotFactory getAiBotFactory() {
        return aiBotFactory;
    }

    public ResourceFactory getResourceFactory() {
        return resourceFactory;
    }
}
