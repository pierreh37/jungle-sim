package fr.rphstudio.jungle.simulation.core.actuator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ActuatorUtil {

    public void addActuator(Map<Class,List<Actuator>> actuators, Actuator actuator){
        List<Actuator> list = actuators.get(actuator.getClass());
        if(list==null){
            list = new ArrayList<>();
            actuators.put(actuator.getClass(),list);
        }
        list.add(actuator);
    }

    public <T extends Actuator> T getFirstActuator(Map<Class,List<Actuator>> actuators, Class<T> clazz){
        if(actuators.get(clazz)==null){
            return null;
        }
        if(actuators.get(clazz).size()==0){
            return null;
        }
        return (T) actuators.get(clazz).get(0);
    }

}
