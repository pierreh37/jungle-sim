package fr.rphstudio.jungle.simulation.core.StateRule;

import fr.rphstudio.jungle.simulation.core.bot.AiBot;

public interface BotRule {
    public void step(AiBot bot);
}
