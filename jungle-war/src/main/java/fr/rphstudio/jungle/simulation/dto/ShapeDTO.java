package fr.rphstudio.jungle.simulation.dto;

import java.io.Serializable;

public class ShapeDTO implements Serializable{
    public float posx,posy;
    public float size;
    public String shape;
    public float r;
    public float g;
    public float b;
    private String id;

    public ShapeDTO(float posx, float posy, float size, String shape, float r, float g, float b,String id) {
        this.posx = posx;
        this.posy = posy;
        this.size = size;
        this.shape = shape;
        this.r = r;
        this.g = g;
        this.b = b;
        this.id = id;
    }

    public float getPosx() {
        return posx;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPosx(float posx) {
        this.posx = posx;
    }

    public float getPosy() {
        return posy;
    }

    public void setPosy(float posy) {
        this.posy = posy;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public float getG() {
        return g;
    }

    public void setG(float g) {
        this.g = g;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }
}
