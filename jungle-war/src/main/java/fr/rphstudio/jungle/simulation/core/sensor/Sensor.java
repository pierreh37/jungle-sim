package fr.rphstudio.jungle.simulation.core.sensor;

import org.tensorflow.Tensor;

public interface Sensor {
    public default String getSensorType() {
         return this.getClass().getSimpleName();
    }

    public Tensor getValue();
}
