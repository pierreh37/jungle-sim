package fr.rphstudio.jungle.simulation.core.sensor;

import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.simulation.EnvironmentAccessor;
import fr.rphstudio.jungle.simulation.core.resource.Resource;
import fr.rphstudio.jungle.simulation.core.state.BodyState;
import org.tensorflow.Tensor;

public class NearestResourceDirection implements Sensor {

    private BodyState bodyState;
    private EnvironmentAccessor world;

    public NearestResourceDirection(BodyState bodyState, EnvironmentAccessor world) {
        this.bodyState = bodyState;
        this.world = world;
    }

    public Vector2 getNearestResourceDirection() {
        Vector2 nearest = new Vector2();
        float best = Float.MAX_VALUE;
        for (Resource resource : world.getResources()) {
            Vector2 bodyPosition = bodyState.getBody().getPosition();
            Vector2 resourcePosition = resource.getBody().getPosition();
            float dst = bodyPosition.dst(resourcePosition);
            if (dst < best) {
                nearest = resourcePosition.cpy();
                best = dst;
            }
        }
        return nearest;
    }

    @Override
    public Tensor getValue() {
        return null;
    }
}
