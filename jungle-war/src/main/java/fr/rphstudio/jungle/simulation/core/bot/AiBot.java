package fr.rphstudio.jungle.simulation.core.bot;

import fr.rphstudio.jungle.simulation.core.actuator.Actuator;
import fr.rphstudio.jungle.simulation.core.ai.AI;
import fr.rphstudio.jungle.simulation.core.sensor.Sensor;
import fr.rphstudio.jungle.simulation.core.state.State;

import java.util.List;
import java.util.Map;

public class AiBot {
    private AI ai;
    Map<Class,List<Sensor>> sensors;
    Map<Class,List<Actuator>> actuators;
    Map<Class,State> states;

    public AiBot(AI ai,Map<Class, List<Sensor>> sensors, Map<Class, List<Actuator>> actuators, Map<Class,State> states) {
        this.ai = ai;
        this.sensors = sensors;
        this.actuators = actuators;
        this.states = states;
    }

    public void step() {
        ai.step(sensors,actuators);
    }

    public Map<Class, List<Sensor>> getSensors() {
        return sensors;
    }

    public Map<Class, List<Actuator>> getActuators() {
        return actuators;
    }

    public Map<Class,State> getStates() {
        return states;
    }
}
