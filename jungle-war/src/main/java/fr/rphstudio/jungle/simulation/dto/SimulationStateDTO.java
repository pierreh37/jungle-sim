package fr.rphstudio.jungle.simulation.dto;

import java.io.Serializable;
import java.util.List;

public class SimulationStateDTO implements Serializable {
    List<ShapeDTO> shapeList;

    public SimulationStateDTO(List<ShapeDTO> shapeList) {
        this.shapeList = shapeList;
    }

    public List<ShapeDTO> getShapeList() {
        return shapeList;
    }

    public void setShapeList(List<ShapeDTO> shapeList) {
        this.shapeList = shapeList;
    }
}
