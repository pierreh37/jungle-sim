package fr.rphstudio.jungle.simulation.core.resource;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import fr.rphstudio.jungle.simulation.EnvironmentAccessor;
import fr.rphstudio.jungle.simulation.dto.Shape;

import java.util.Random;

public class ResourceFactory {

    EnvironmentAccessor world;

    float bodySize = 20f;
    Random random = new Random();
    public ResourceFactory(EnvironmentAccessor world) {
        this.world = world;
    }

    public Resource create(){
        Vector2 vector2 = new Vector2(random.nextInt((int) (world.getWidth() - bodySize)) + bodySize, random.nextInt((int) (world.getHeight() - bodySize)) + bodySize);
        return create(vector2);

    }

    public Resource create(Vector2 initPosition){
        Shape shape = new Shape(initPosition.cpy(),bodySize, Shape.CIRCLE, Color.BLUE);

        // Now create a BodyDefinition.  This defines the physics objects type and position in the simulation
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        // We are going to use 1 to 1 dimensions.  Meaning 1 in physics engine is 1 pixel
        // Set our body to the same position as our sprite
        bodyDef.position.set(initPosition.x, initPosition.y);

        // Create a body in the world using our definition
        Body body = world.getPhysicWorld().createBody(bodyDef);

        // Now define the dimensions of the physics shape
        CircleShape polyShape = new CircleShape();
        // We are a box, so this makes sense, no?
        // Basically set the physics polygon to a box with the same dimensions as our sprite
        polyShape.setRadius(bodySize/2f);

        // FixtureDef is a confusing expression for physical properties
        // Basically this is where you, in addition to defining the shape of the body
        // you also define it's properties like density, restitution and others we will see shortly
        // If you are wondering, density and area are used to calculate over all mass
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polyShape;
        fixtureDef.density = 1f;

        Fixture fixture = body.createFixture(fixtureDef);

        // Shape is the only disposable of the lot, so get rid of it
        polyShape.dispose();
        return new Resource(shape,body,fixture);
    }
}
