package fr.rphstudio.jungle.simulation.core.state;

import java.util.List;
import java.util.Map;

public class StateUtil {
    public <T extends State> T addState(Map<Class,State> states, T state, Class<T> clazz){
        states.put(clazz,state);
        return state;
    }


    public <T extends State> T getState(Map<Class,State> states, Class<T> state){
        return (T) states.get(state);
    }
}
