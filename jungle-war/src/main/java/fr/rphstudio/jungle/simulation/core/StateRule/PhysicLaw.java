package fr.rphstudio.jungle.simulation.core.StateRule;

import com.badlogic.gdx.physics.box2d.*;
import fr.rphstudio.jungle.simulation.EnvironmentAccessor;
import fr.rphstudio.jungle.simulation.core.bot.AiBot;
import fr.rphstudio.jungle.simulation.core.state.BodyState;
import fr.rphstudio.jungle.simulation.core.state.StateUtil;

public class PhysicLaw implements BotRule,WorldRule {
    private EnvironmentAccessor ea;
    private World world;
    int wallSize = 100;
    StateUtil stateUtil = new StateUtil();


    public PhysicLaw(EnvironmentAccessor ea) {
        this.ea = ea;
        world = ea.getPhysicWorld();
    }

    @Override
    public void step(AiBot bot) {
        //update shape render with physic render
        BodyState bodyState = stateUtil.getState(bot.getStates(), BodyState.class);
        bodyState.getShape().position.set(bodyState.getBody().getPosition().x, bodyState.getBody().getPosition().y);
    }

    @Override
    public void create(EnvironmentAccessor ea) {
        {
            int height = ea.getHeight();
            int width = ea.getWidth();
            //left physic wall
            mkWall(-(wallSize/2f), +(height/2f),wallSize,height);
            //right physic wall
            mkWall(width+(wallSize/2f), +(height/2f),wallSize,height);
            //top physic wall
            mkWall((width/2f), height+(wallSize/2f),width,wallSize);
            //down physic wall
            mkWall((width/2f), -(wallSize/2f),width,wallSize);
        }
    }

    @Override
    public void step(EnvironmentAccessor ea) {

    }

    private void mkWall(float x,float y,int width,int height){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x,y);
        Body body = world.createBody(bodyDef);
        PolygonShape polyShape = new PolygonShape();
        polyShape.setAsBox(width/2f,height/2f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polyShape;
        fixtureDef.density = 1f;
        Fixture fixture = body.createFixture(fixtureDef);
        polyShape.dispose();
    }
}
