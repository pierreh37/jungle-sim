package fr.rphstudio.jungle.simulation.core.sensor;

import fr.rphstudio.jungle.simulation.core.state.Hunger;
import org.tensorflow.Tensor;

public class HungerSensor implements Sensor{
    Hunger hunger;

    public HungerSensor(Hunger hunger) {
        this.hunger = hunger;
    }

    public Hunger getHunger() {
        return hunger;
    }

    @Override
    public Tensor getValue() {
        return null;
    }
}
