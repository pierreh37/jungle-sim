package fr.rphstudio.jungle.simulation.core.state;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import fr.rphstudio.jungle.simulation.dto.Shape;

public class BodyState implements State{
    Shape shape;
    Body body;
    Fixture fixture;

    public BodyState(Shape shape, Body body, Fixture fixture) {
        this.shape = shape;
        this.body = body;
        this.fixture = fixture;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }
}
