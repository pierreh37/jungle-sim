package fr.rphstudio.jungle.simulation.core.StateRule;

import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.simulation.EnvironmentAccessor;

public class ResourceRule implements WorldRule {

    @Override
    public void create(EnvironmentAccessor ea) {

    }

    @Override
    public void step(EnvironmentAccessor ea) {
        //first world rule resources must spawn (sun like rule)
        if(ea.getResources().size()<2){
            ea.getResources().add(ea.getResourceFactory().create());
        }
    }
}
