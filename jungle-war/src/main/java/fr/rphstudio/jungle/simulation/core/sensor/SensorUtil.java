package fr.rphstudio.jungle.simulation.core.sensor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SensorUtil {

    public void addSensor(Map<Class,List<Sensor>> sensors,Sensor sensor){
        List<Sensor> list = sensors.get(sensor.getClass());
        if(list==null){
            list = new ArrayList<>();
            sensors.put(sensor.getClass(),list);
        }
        list.add(sensor);
    }

    public <T extends Sensor> T getFirstSensor(Map<Class,List<Sensor>> sensors, Class<T> clazz){
        if(sensors.get(clazz)==null){
            return null;
        }
        if(sensors.get(clazz).size()==0){
            return null;
        }
        return (T) sensors.get(clazz).get(0);
    }

}
