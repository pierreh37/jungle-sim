package fr.rphstudio.jungle.simulation.core.actuator;

import org.tensorflow.Tensor;

public interface Actuator {
    public void doAction(Tensor data);
}
