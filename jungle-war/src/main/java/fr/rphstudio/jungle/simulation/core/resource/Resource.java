package fr.rphstudio.jungle.simulation.core.resource;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import fr.rphstudio.jungle.simulation.dto.Shape;

public class Resource {
    private Shape shape;
    Body body;
    Fixture fixture;

    public Resource(Shape shape, Body body, Fixture fixture) {
        this.setShape(shape);
        this.body = body;
        this.fixture = fixture;
    }

    public void step() {
        //update shape render with pysic render
        getShape().position.set(body.getPosition().x, body.getPosition().y);
        //Do some IA stuff here
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Body getBody() {
        return body;
    }

    public Fixture getFixture() {
        return fixture;
    }
}
