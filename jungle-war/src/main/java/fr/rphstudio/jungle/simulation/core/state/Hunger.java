package fr.rphstudio.jungle.simulation.core.state;

public class Hunger implements State{
    float hunger;//-1 hungry 1 not hungry

    public float getHunger() {
        return hunger;
    }

    public void setHunger(float hunger) {
        this.hunger = hunger;
    }

    public void add(float v) {
        hunger+=v;
    }
}
