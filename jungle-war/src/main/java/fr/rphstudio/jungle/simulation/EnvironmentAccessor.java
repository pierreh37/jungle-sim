package fr.rphstudio.jungle.simulation;

import com.badlogic.gdx.physics.box2d.World;
import fr.rphstudio.jungle.simulation.core.bot.AiBot;
import fr.rphstudio.jungle.simulation.core.bot.AiBotFactory;
import fr.rphstudio.jungle.simulation.core.resource.Resource;
import fr.rphstudio.jungle.simulation.core.resource.ResourceFactory;

import java.util.List;

public interface EnvironmentAccessor {
    public List<AiBot> getBots();
    public List<Resource> getResources();
    public World getPhysicWorld();
    public int getWidth();
    public int getHeight();
    public AiBotFactory getAiBotFactory();
    public ResourceFactory getResourceFactory();
}
