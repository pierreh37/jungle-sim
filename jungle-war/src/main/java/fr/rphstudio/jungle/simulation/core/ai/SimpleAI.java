package fr.rphstudio.jungle.simulation.core.ai;

import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.simulation.core.actuator.Actuator;
import fr.rphstudio.jungle.simulation.core.actuator.ActuatorUtil;
import fr.rphstudio.jungle.simulation.core.actuator.Eat;
import fr.rphstudio.jungle.simulation.core.actuator.Move;
import fr.rphstudio.jungle.simulation.core.sensor.*;

import java.util.List;
import java.util.Map;

public class SimpleAI implements AI{
    SensorUtil sensorUtil = new SensorUtil();
    ActuatorUtil actuatorUtil = new ActuatorUtil();

    @Override
    public void step(Map<Class, List<Sensor>> sensors, Map<Class, List<Actuator>> actuators) {
        //for neural network collect all data and put it in an array
        NearestResourceDirection nearestResourceDirection = sensorUtil.getFirstSensor(sensors,NearestResourceDirection.class);
        HungerSensor hunger = sensorUtil.getFirstSensor(sensors,HungerSensor.class);
        LocationAware locationAware = sensorUtil.getFirstSensor(sensors,LocationAware.class);
        Move move = actuatorUtil.getFirstActuator(actuators,Move.class);
        Eat eat = actuatorUtil.getFirstActuator(actuators,Eat.class);

        //do some ia intelligence
        Vector2 nearestResourceDirectionVector = nearestResourceDirection.getNearestResourceDirection();
        Vector2 bodyPosition = locationAware.getLocation();
        Vector2 nor = nearestResourceDirectionVector.sub(bodyPosition).nor();
        move.move(nor);
        eat.eat();
    }
}
