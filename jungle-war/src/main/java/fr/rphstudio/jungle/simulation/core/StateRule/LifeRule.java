package fr.rphstudio.jungle.simulation.core.StateRule;

import fr.rphstudio.jungle.simulation.EnvironmentAccessor;

public class LifeRule implements WorldRule {
    int generation = 0;

    @Override
    public void create(EnvironmentAccessor ea) {

    }

    @Override
    public void step(EnvironmentAccessor ea) {
        //second world rule if there is no life 2 new life shal be created (universe generation must update)
        if(ea.getBots().size()<1){
            generation++;
            System.out.println("generation : "+generation);
            ea.getBots().add(ea.getAiBotFactory().create());
            ea.getBots().add(ea.getAiBotFactory().create());
        }
    }
}
