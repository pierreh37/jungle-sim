package fr.rphstudio.jungle.simulation.core.StateRule;

import fr.rphstudio.jungle.simulation.EnvironmentAccessor;
import fr.rphstudio.jungle.simulation.core.bot.AiBot;
import fr.rphstudio.jungle.simulation.core.state.Hunger;
import fr.rphstudio.jungle.simulation.core.state.StateUtil;

import java.util.ListIterator;

public class DieFromHunger implements BotRule {
    private EnvironmentAccessor world;
    StateUtil stateUtil = new StateUtil();

    public DieFromHunger(EnvironmentAccessor world) {
        this.world = world;
    }

    @Override
    public void step(AiBot bot) {
        Hunger hunger = stateUtil.getState(bot.getStates(), Hunger.class);
        if(hunger.getHunger()<=-1){
            world.getAiBotFactory().deleteAsync(bot);
        }
    }
}
