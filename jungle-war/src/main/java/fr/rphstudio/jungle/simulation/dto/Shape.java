package fr.rphstudio.jungle.simulation.dto;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class Shape {
    public static final String lINE = "line";
    public static final String SQUARE = "square";
    public static final String CIRCLE = "circle";
    public static final String TRIANGLE = "triangle";
    public static int currentGlobalID = 0;
    public String id;
    public Vector2 position;
    public float size;
    public String shape;
    public Color color;

    public Shape(Vector2 position, float size, String shape, Color color) {
        this.id = currentGlobalID+"";
        currentGlobalID++;
        this.position = position;
        this.size = size;
        this.shape = shape;
        this.color = color;
    }

    public ShapeDTO convert(){
        return new ShapeDTO(position.x,position.y,size,shape,color.r,color.g,color.b,id);
    }
}
