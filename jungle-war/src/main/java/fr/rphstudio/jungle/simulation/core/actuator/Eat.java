package fr.rphstudio.jungle.simulation.core.actuator;

import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.simulation.EnvironmentAccessor;
import fr.rphstudio.jungle.simulation.core.resource.Resource;
import fr.rphstudio.jungle.simulation.core.state.BodyState;
import fr.rphstudio.jungle.simulation.core.state.Hunger;
import org.tensorflow.Tensor;

import java.util.ListIterator;

public class Eat implements Actuator{
    BodyState bodyState;
    Hunger hunger;
    private EnvironmentAccessor world;
    float eatRadius = 20;

    public Eat(BodyState bodyState, Hunger hunger, EnvironmentAccessor world) {
        this.bodyState = bodyState;
        this.hunger = hunger;
        this.world = world;
    }

    public void eat(){
        Vector2 bodyPosition = bodyState.getBody().getPosition();
        ListIterator<Resource> iterator = world.getResources().listIterator();
        //hunger.add(-0.1f);
        while (iterator.hasNext()) {
            Resource resource = iterator.next();
            Vector2 resourcePosition = resource.getBody().getPosition();
            if(bodyPosition.dst(resourcePosition)<eatRadius+(bodyState.getShape().size/2f)){
                hunger.add(1.0f);
                iterator.remove();//remove resource from world
            }
        }
    }

    @Override
    public void doAction(Tensor data) {

    }
}
