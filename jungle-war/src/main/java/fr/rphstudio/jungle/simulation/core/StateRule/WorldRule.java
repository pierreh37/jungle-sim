package fr.rphstudio.jungle.simulation.core.StateRule;

import fr.rphstudio.jungle.simulation.EnvironmentAccessor;

public interface WorldRule {
    public void create(EnvironmentAccessor ea);
    public void step(EnvironmentAccessor ea);
}
