package fr.rphstudio.jungle.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.github.czyzby.websocket.CommonWebSockets;
import com.google.gson.Gson;
import fr.rphstudio.jungle.Main;
import fr.rphstudio.jungle.dto.JSONHelper;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.out.println("Desktop application launched");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		CommonWebSockets.initiate();
		final Gson g = new Gson();

        Main main = new Main();
        main.setJsonHelper(new JSONHelper() {
            @Override
            public <T> T parse(String json, Class<T> clazz) {
                return g.fromJson(json,clazz);
            }
        });
		new LwjglApplication(main, config);
	}
}
