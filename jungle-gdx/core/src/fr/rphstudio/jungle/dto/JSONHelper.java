package fr.rphstudio.jungle.dto;

public interface JSONHelper {
    public <T> T parse(String json,Class<T> clazz);
}
