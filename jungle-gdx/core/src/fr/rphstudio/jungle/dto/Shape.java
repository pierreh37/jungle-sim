package fr.rphstudio.jungle.dto;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class Shape {
    public static final String lINE = "line";
    public static final String SQUARE = "square";
    public static final String CIRCLE = "circle";
    public static final String TRIANGLE = "triangle";
    public Vector2 position;
    public float size;
    public String shape;
    public Color color;

    public Shape(Vector2 position, float size, String shape, Color color) {
        this.position = position;
        this.size = size;
        this.shape = shape;
        this.color = color;
    }
}
