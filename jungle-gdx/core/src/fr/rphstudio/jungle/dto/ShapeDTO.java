package fr.rphstudio.jungle.dto;

import sk.seges.acris.json.client.annotation.Field;
import sk.seges.acris.json.client.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class ShapeDTO implements Serializable{
    @Field
    public float posx,posy;
    @Field
    public float size;
    @Field
    public String shape;
    @Field
    public float r,g,b;

    public ShapeDTO(float posx, float posy, float size, String shape, float r, float g, float b) {
        this.posx = posx;
        this.posy = posy;
        this.size = size;
        this.shape = shape;
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public float getPosx() {
        return posx;
    }

    public void setPosx(float posx) {
        this.posx = posx;
    }

    public float getPosy() {
        return posy;
    }

    public void setPosy(float posy) {
        this.posy = posy;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public float getG() {
        return g;
    }

    public void setG(float g) {
        this.g = g;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }
}
