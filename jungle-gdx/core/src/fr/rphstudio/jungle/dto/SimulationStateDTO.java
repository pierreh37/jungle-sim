package fr.rphstudio.jungle.dto;

import sk.seges.acris.json.client.annotation.Field;
import sk.seges.acris.json.client.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class SimulationStateDTO implements Serializable {
    @Field
    List<ShapeDTO> shapeList;

    public SimulationStateDTO(List<ShapeDTO> shapeList) {
        this.shapeList = shapeList;
    }

    public List<ShapeDTO> getShapeList() {
        return shapeList;
    }

    public void setShapeList(List<ShapeDTO> shapeList) {
        this.shapeList = shapeList;
    }
}
