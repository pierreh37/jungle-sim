package fr.rphstudio.jungle;

import com.badlogic.gdx.ApplicationAdapter;
import fr.rphstudio.jungle.dto.JSONHelper;
import fr.rphstudio.jungle.renderer.WebRenderer;

public class Main extends ApplicationAdapter {
	WebRenderer simulation;
    private JSONHelper jsonHelper;

    public void setJsonHelper(JSONHelper jsonHelper) {
        this.jsonHelper = jsonHelper;
    }

    @Override
	public void create () {
        simulation = new WebRenderer(jsonHelper);
        simulation.create();
	}

	@Override
	public void render () {
        simulation.render();
	}
	
	@Override
	public void dispose () {
        simulation.dispose();
	}
}
