package fr.rphstudio.jungle.renderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import fr.rphstudio.jungle.dto.JSONHelper;
import fr.rphstudio.jungle.dto.Shape;
import fr.rphstudio.jungle.dto.ShapeDTO;
import fr.rphstudio.jungle.dto.SimulationStateDTO;
import fr.rphstudio.jungle.renderer.Websocket.STOMP;
import sk.seges.acris.json.client.IJsonizer;
import sk.seges.acris.json.client.JsonizerBuilder;

import java.util.ArrayList;
import java.util.List;

public class WebRenderer {

    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer ;
    private fr.rphstudio.jungle.renderer.Websocket.STOMP stomp = new STOMP();
    private SimulationStateDTO simulationState = new SimulationStateDTO(new ArrayList<ShapeDTO>());
    private JSONHelper jsonHelper;

    public WebRenderer(JSONHelper jsonHelper) {

        this.jsonHelper = jsonHelper;
    }

    public void create () {
        shapeRenderer = new ShapeRenderer();
        //Above code will draw everything in your game on 800*400 virtual screen (in pixel) and fit, stretch, fill etc (see viewports ) on device's screen, independent of device screen size.
        camera = new OrthographicCamera();
        camera.setToOrtho(false,640,480);

        //stomp.connectSocket("ws://127.0.0.1:8080/jungle-war/gs-guide-websocket");
        stomp.connectSocket("ws://junglewar.test.rphstudio.net/jungle-war/gs-guide-websocket");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        STOMP.StompChanel chanel = stomp.createChanel("/app/jungle/control", "/topic/jungle/state", new STOMP.StompListener() {
            @Override
            public void message(String jsonData) {
                SimulationStateDTO person = jsonHelper.parse(jsonData,SimulationStateDTO.class);
                simulationState = person;// asignation are sychronized
            }
        });
        //chanel.sendJson("{\"name\":\"lol\"}");
    }

    public void render () {
        List<ShapeDTO> shapeList = simulationState.getShapeList();
        List<Shape> shapes = new ArrayList<>();
        for (ShapeDTO shapeDTO : shapeList) {
            shapes.add(new Shape(new Vector2(shapeDTO.posx,shapeDTO.posy),shapeDTO.size,shapeDTO.shape,new Color(shapeDTO.r,shapeDTO.g,shapeDTO.b,1.0f)));
        }

        /* x -> and y î */
        Gdx.gl.glClearColor(0, 0.5f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        for (Shape shape : shapes) {
            shapeRenderer.setProjectionMatrix(camera.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            shapeRenderer.setColor(shape.color);
            switch (shape.shape){
                case Shape.CIRCLE:
                    shapeRenderer.circle(shape.position.x,  shape.position.y, shape.size/2f);
                    break;
                case Shape.lINE:
                    //shapeRenderer.line(shape.position.x, shape.position.y, shape.position.x, shape.position.y+shape.size);
                    //break;
                    throw new Error();
                case Shape.SQUARE:
                    shapeRenderer.rect(shape.position.x-(shape.size/2f), shape.position.y-(shape.size/2f), shape.size, shape.size);
                    break;
                case Shape.TRIANGLE:
                    throw new Error();
                default:
                    throw new Error();
            }
            shapeRenderer.end();
        }


    }

    public void dispose () {
    }

}
