package fr.rphstudio.jungle.renderer.Websocket;

import com.github.czyzby.websocket.WebSocket;
import com.github.czyzby.websocket.WebSocketAdapter;
import com.github.czyzby.websocket.WebSockets;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class STOMP {
    String version = "1.1";
    /*String LF = Character.toString ((char) 0x0A);
    String NULL = Character.toString ((char) 0x00);*/
    String LF = "\\n";
    String NULL = "\\u0000";
    private RandomString randomStringPrefix = new RandomString(3);
    private RandomString randomStringSufix = new RandomString(8);
    HashMap<String, StompChanel> chanels = new HashMap<>();
    WebSocket webSocket;

    public String getURIwithSessionID(String uri) {
        uri += "/" + randomStringPrefix.nextString() + "/" + randomStringSufix.nextString() + "/websocket";
        return uri;
    }

    public interface StompListener {
        public void message(String jsonData);
    }

    public class StompChanel {
        String chanelSend;
        String chanelReceive;
        StompListener listener;

        public StompChanel(String chanelSend, String chanelReceive, StompListener listener) {
            this.chanelSend = chanelSend;
            this.chanelReceive = chanelReceive;
            this.listener = listener;
        }

        public void sendJson(String jsonData) {
            //"{\"name\":\"lol\"}"
            webSocket.send(send(chanelSend, jsonData));
        }
    }


    public StompChanel createChanel(String chanelSend, final String chanelReceive, StompListener listener) {
        final StompChanel stompChanel = new StompChanel(chanelSend, chanelReceive, listener);
        webSocket.send(subscribe(chanelReceive));
        chanels.put(chanelReceive, stompChanel);
        return stompChanel;
    }

    public void connectSocket(String uri) {
        String websocketAddress = getURIwithSessionID(uri);
        System.out.println("connecting to :" + websocketAddress);
        webSocket = WebSockets.newSocket(websocketAddress);
        webSocket.addListener(new WebSocketAdapter() {
            @Override
            public boolean onMessage(WebSocket webSocket, String packet) {
                try {
                    swit:
                    switch (packet.charAt(0)) {
                        case 'o':
                            System.out.println("ws connected");
                            webSocket.send(connect());
                            break;
                        case 'h':
                            System.out.println("heart beat");
                            break;
                        case 'a':
                            packet = packet.substring(3, packet.length() - 8);
                            String[] split = packet.split("\\\\n");
                            String type = split[0];
                            switch (type) {
                                case "CONNECTED":
                                    break;
                                case "MESSAGE":
                                    if (!split[2].equalsIgnoreCase("content-type:application/json;charset=UTF-8")) {
                                        break swit;
                                    }
                                    String destination = split[1].split(":")[1];
                                    String jsonData = split[split.length - 1];
                                    jsonData = jsonData.replaceAll("\\\\", "");
                                    chanels.get(destination).listener.message(jsonData);
                                    break;
                                default:
                                    System.out.println(packet);
                                    System.err.println("WS unkown message type");
                            }
                            break;
                        default:
                            System.err.println("unkown ws message");
                            break;
                    }
                    return true;
                } catch (Exception ex) {
                    System.err.println(ex);
                    return false;
                }
            }
        });

        webSocket.connect();
    }

    public String webSocketWrapString(String in) {
        in = in.replaceAll("\n", "\\\\n");
        return "[\"" + in + NULL + "\"]";
    }

    public String marchallFrame(String command, List<String> header, String body) {
        String ret = command + LF;
        for (String h : header) {
            ret += h + LF;
        }
        ret += LF;
        if (body != null) {
            ret += body;
        }
        ret += NULL;
        ret = "[\"" + ret + "\"]";
        System.out.println(">>>" + ret);
        return ret;
        //return ret.getBytes(Charset.forName("ASCII"));
    }

    public Map<String, String> fromList(String... ins) {
        Map<String, String> ret = new HashMap<>();
        for (String in : ins) {
            String[] strings = in.split(":");
            ret.put(strings[0], strings[1]);
        }
        return ret;
    }

    public String connect() {
        return marchallFrame("CONNECT", Arrays.asList("accept-version:1.1", "heart-beat:10000,10000"), null);
    }

    public String subscribe(String chanel) {
        return marchallFrame("SUBSCRIBE", Arrays.asList("id:sub-0", "destination:" + chanel), null);
    }

    public String send(String chanel, String jsonData) {
        String s = jsonData.replaceAll("\\\"", "\\\\\\\"");
        return marchallFrame("SEND",
                Arrays.asList("destination:/app/hello", "content-length:" + jsonData.length()),
                s);
    }
}
