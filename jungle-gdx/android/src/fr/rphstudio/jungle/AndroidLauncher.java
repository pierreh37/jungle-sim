package fr.rphstudio.jungle;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import fr.rphstudio.jungle.Main;
import fr.rphstudio.jungle.dto.JSONHelper;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new Main(new JSONHelper() {
			@Override
			public <T> T parse(String json, Class<T> clazz) {
				return null;
			}
		}), config);
	}
}
