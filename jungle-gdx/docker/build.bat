rem cd ../jungle
rem call gradlew html:dist
rem cd ../docker
rmdir /s/q .\www\
xcopy /s .\..\jungle\html\build\dist .\www\
docker build -t webserver-jungle .
docker image save webserver-jungle:latest -o webserver-jungle.docker_image